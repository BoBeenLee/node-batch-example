#!/bin/bash

ACCOUNT=076532408058 # REMEMBER TO REPLACE THE AWS ACCOUNT ID
DOCKER_CONTAINER=node-batch
REPO=$ACCOUNT.dkr.ecr.ap-northeast-2.amazonaws.com/$DOCKER_CONTAINER
TAG=build-$(date -u "+%Y-%m-%d")

echo "Building Docker Image..."
docker build -t $DOCKER_CONTAINER .

echo "Authenticating against AWS ECR..."
eval $(aws ecr get-login --no-include-email --region ap-northeast-2 --profile fitsme.dev)

echo "Tagging $REPO..."
docker tag $DOCKER_CONTAINER:latest $REPO:$TAG
docker tag $DOCKER_CONTAINER:latest $REPO:latest

echo "Deploying to AWS ECR"
# docker login -u AWS -p <password> <aws_account_id>.dkr.ecr.<region>.amazonaws.com
docker push $REPO
